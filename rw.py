"""Module for file related operations."""

from rich.markdown import Markdown


def read_md(_path: str):
    """Membaca file markdown."""
    with open(_path, 'r') as _file:
        return Markdown(_file.read())


def read_f(_path: str):
    """Membaca file text."""
    with open(_path, 'r') as _file:
        return _file.read()
