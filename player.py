"""Module for player related class in the game."""

from card import Card
from random import choice
from rich.table import Table
from rich import box


class Player:
    """Kelas utama untuk pemain."""

    def __init__(self, _total_cards: int, _turn: int):
        """Inisialisasi objek pemain dengan hp 100.

        Constructor objek Player membutuh kan 2 input argumen:
        _total_cards = untuk memberikan atribut total_cards pada objek
        _turn = untuk memberikan attribut turn pada objek

        kedua parameter tersebut dihasilkan dan dimiliki oleh objek game
        """
        self.health_point = 100
        self.total_cards = _total_cards
        self.cards = []
        self.turn = _turn
        self.gen_cards()
        self.chosen_card = choice(self.cards)

    def gen_cards(self):
        """Generate kartu untuk pemain."""
        _cards = []

        while _cards.__len__() < self.total_cards:
            card = Card()
            _cards.append(card)
            card.set_id(_cards.index(card))

        self.cards = _cards

    def deduct_turn(self):
        """Mengurangi giliran pemain."""
        if self.turn == 0:
            self.turn = 0
        else:
            self.turn -= 1

    def set_total_cards(self, _total_cards: int):
        """Mengatur total kartu."""
        self.total_cards = _total_cards

    def set_turn(self, _turn: int):
        """Mengatur giliran."""
        self.turn = _turn

    def destroy_card(self, _card: Card):
        """Menghapus kartu dari daftar kartu pemain."""
        self.cards.remove(_card)

    def deduct_hp(self, _hp_to_deduct: int):
        """Mengurangi HP pemain."""
        self.health_point -= _hp_to_deduct

    def set_chose_card(self, _card: int):
        """Memilih kartu."""
        for card in self.cards:
            if _card == card.card_id:
                self.chosen_card = card
                break

    def __rich_repr__(self):
        """Memberikan informasi mengenai objek yang diberikan."""
        yield "Health", self.health_point
        yield "Turn", self.turn
        yield "Cards", self.cards
        yield "Active Card", self.chosen_card
        yield "Active Card State", self.chosen_card.state


class Human(Player):
    """Kelas untuk pemain manusia."""

    def __init__(self, _total_cards: int, _turn: int):
        """Inisialisasi objeck pemain manusia."""
        super().__init__(_total_cards, _turn)

    def gen_cards_table(self):
        """Membuat table kartu untuk menampilkan daftar kartu yang dimiliki pemain."""
        self.cards_table = Table(box=box.ASCII, title="Kartu Pemain")

        self.cards_table.add_column("No")
        self.cards_table.add_column("Attack", style="red")
        self.cards_table.add_column("Defence", style="green")

        for card in self.cards:
            self.cards_table.add_row(
                f"{card.card_id}",
                f"{card.attack}",
                f"{card.defence}"
            )

    def change_card_state(self, _state: int):
        """Mengatur mode kartu."""
        if _state == 0:
            self.chosen_card.set_state(False)
        else:
            self.chosen_card.set_state(True)


class Computer(Player):
    """Kelas untuk pemain komputer."""

    def __init__(self, _total_cards: int, _turn: int):
        """Inisialisasi objeck pemain komputer."""
        super().__init__(_total_cards, _turn)

    def chose_stategy(self):
        """Memilih strategi bagi komputer."""
        possible_strategy = (False, True)
        self.strategy = choice(possible_strategy)

    def chose_card(self):
        """Memilih kartu."""
        cards_atk_points = {}
        cards_dfs_points = {}

        for card in self.cards:
            cards_atk_points[card.card_id] = card.attack
            cards_dfs_points[card.card_id] = card.defence

        if self.strategy:
            for c_id in cards_atk_points.keys():
                highest_atk_point = max(cards_atk_points.values())
                c_atk = cards_atk_points.get(c_id)

                if c_atk == highest_atk_point:
                    self.set_chose_card(c_id)
                    break

        else:
            for c_id in cards_dfs_points.keys():
                highest_dfs_point = max(cards_dfs_points.values())
                c_dfs = cards_dfs_points.get(c_id)

                if c_dfs == highest_dfs_point:
                    self.set_chose_card(c_id)
                    break

    def change_card_state(self):
        """Mengatur mode kartu."""
        self.chosen_card.set_state(self.strategy)
