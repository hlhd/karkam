"""Module for all game related operations."""

import player
from rich.prompt import IntPrompt
from random import randint
from rich import print as rprint
from rich.table import Table
from rich import box
from console import console
import rw


class Game:
    """Menyimpan semua fungsi yang berkaitan dengan jalannya permainan."""

    def __init__(self, _run: bool):
        """Inisialisasi objek Game."""
        self.run = _run
        self.end = False
        self.total_cards = randint(5, 10)
        self.turn = 10 if self.total_cards == 10 else self.total_cards + 1

    def gen_info_table(self, _h: player.Human):
        """Membuat tabel informasi mengenai status pemain."""
        self.info_table = Table(
            "HP",
            "Giliran",
            "Total Kartu",
            box=box.ASCII,
            title="Info"
        )

        self.info_table.add_row(
            f"{_h.health_point}",
            f"{_h.turn}",
            f"{len(_h.cards)}"
        )

    def ask_player_to_chose_card(self, _player: player.Human):
        """Meminta pemain untuk memilih kartu."""
        cards = []
        for card in _player.cards:
            cards.append(card.card_id)

        while True:
            chosen_card = IntPrompt().ask(f"Pilih kartu yang ingin Anda Gunakan {cards}")

            if chosen_card not in cards:
                rprint("[bold red]Mohon pilih kartu yang tersedia[/bold red]")
            else:
                break

        _player.set_chose_card(chosen_card)

    def ask_player_to_set_card_state(self, _player: player.Human):
        """Meminta pemain untuk mengatur mode kartu."""
        possible_state = [0, 1]

        while True:
            state = IntPrompt().ask(
                "Atur mode kartu yang Anda gunakan [0: bertahan, 1: menyerang]",
                default=0
            )

            if state not in possible_state:
                rprint("[bold red]Mohon pilih mode yang tersedia[/bold red]")
            else:
                break

        _player.change_card_state(state)

    def gen_arena(self, _h: player.Human, _c: player.Computer):
        """Membuat tabel informasi arena."""
        self.arena = Table(
            "Peserta",
            "Status Kartu",
            "Mode Kartu",
            box=box.ASCII,
            title="Arena"
        )

        self.arena.add_row(
            f"Komputer: {_c.health_point} HP",
            f"ATK: {_c.chosen_card.attack}, DFS: {_c.chosen_card.defence}",
            f"Menyerang: {'ya' if _c.chosen_card.state else 'tidak'}"
        )

        self.arena.add_row(
            f"Pemain: {_h.health_point} HP",
            f"ATK: {_h.chosen_card.attack}, DFS: {_h.chosen_card.defence}",
            f"Menyerang: {'ya' if _h.chosen_card.state else 'tidak'}"
        )

    def battle(self, _h: player.Human, _c: player.Computer):
        """Menjalankan pertempuran."""
        if _h.chosen_card.state and _c.chosen_card.state:
            self.both_attack(_h=_h, _c=_c)
        elif not _h.chosen_card.state and not _c.chosen_card.state:
            rprint("Wooohhh!!!.. Kedua pemain memilih [green]bertahan[/green]!")
            rprint("Sayang sekali, tidak terjadi apa - apa")
            rprint("Mari kita lanjutkan")
        elif not _h.chosen_card.state:
            self.player_defence(_h=_h, _c=_c)
        else:
            self.comp_defence(_h=_h, _c=_c)

    def both_attack(self, _h: player.Human, _c: player.Computer):
        """Operasi jika kedua pemain memilih menyerang."""
        rprint("Kedua kartu menyerang")
        mn = min(_h.chosen_card.attack, _c.chosen_card.attack)
        mx = max(_h.chosen_card.attack, _c.chosen_card.attack)
        sub_hp = mx - mn

        attack = min(_h.chosen_card.attack, _c.chosen_card.attack)

        if attack == _h.chosen_card.attack:
            rprint("Kartu pemain kalah!")
            rprint(f"HP pemain akan dikurangi {sub_hp}")
            _h.deduct_hp(sub_hp)
        else:
            rprint("Kartu komputer kalah!")
            rprint(f"HP komputer akan dikurangi {sub_hp}")
            _c.deduct_hp(sub_hp)

    def player_defence(self, _h: player.Human, _c: player.Computer):
        """Operasi jika pemain memilih bertahan."""
        rprint("Player: [green]bertahan[/green], Comp: [red]menyerang[/red]")
        if _h.chosen_card.defence > _c.chosen_card.attack:
            rprint("Kartu bertahan menang!")
            rprint("Kartu menyerang akan dihancurkan")
            _c.destroy_card(_c.chosen_card)
        else:
            rprint("Kartu bertahan kalah!")
            rprint("Kartu bertahan akan dihancurkan")
            _h.destroy_card(_h.chosen_card)

    def comp_defence(self, _h: player.Human, _c: player.Computer):
        """Operasi jika komputer memilih bertahan."""
        rprint("Comp: [green]bertahan[/green], Player: [red]menyerang[/red]")
        if _c.chosen_card.defence > _h.chosen_card.attack:
            rprint("Kartu bertahan menang!")
            rprint("Kartu menyerang akan dihancurkan")
            _h.destroy_card(_h.chosen_card)
        else:
            rprint("Kartu bertahan kalah!")
            rprint("Kartu bertahan akan dihancurkan")
            _c.destroy_card(_c.chosen_card)

    def calculate_winner(self, _h: player.Human, _c: player.Computer):
        """Mencari pemenang."""
        win_hp = max(_h.health_point, _c.health_point)

        if win_hp == _h.health_point:
            self.winner = _h
        else:
            self.winner = _c

    def menu(self, _h: player.Human, _c: player.Computer):
        """Game menu."""
        possible_ans = [0, 1]

        while True:
            ans = IntPrompt().ask(
                "Menu: [0: Keluar, 1: Main]",
                default=1
            )

            if ans not in possible_ans:
                rprint("[bold red]Mohon pilih menu yang tersedia[/bold red]")
            else:
                if ans == 1:
                    self.playing(_h=_h, _c=_c)
                self.run = False if ans == 0 else True
                break

    def check_players_hp(self, _p: player.Player):
        """Mengecek sisa HP pemain."""
        if _p.health_point <= 0:
            console.rule("[bold]Permainan Berakhir")
            rprint(f"{_p.__class__.__name__} dinyatakan [bold]kalah[/bold], karena HP telah mencapai 0")
            self.end = True

    def check_players_turns(self, _p: player.Player):
        """Mengecek sisa giliran pemain."""
        if _p.turn <= 0:
            console.rule("[bold]Permainan Berakhir")
            rprint(f"{_p.__class__.__name__} kehabisan giliran")
            self.end = True

    def check_players_total_cards(self, _p: player.Player):
        """Mengecek total kartu milik pemain."""
        if len(_p.cards) <= 0:
            console.rule("[bold]Permainan Berakhir")
            rprint(f"{_p.__class__.__name__} kehabisan kartu")
            self.end = True

    def declare_winner(self):
        """Menyatakan pemenang."""
        if self.winner.__class__.__name__ == "Human":
            rprint("Pemain menang!")
        else:
            rprint("Komputer menang!")

    def playing(self, _h: player.Human, _c: player.Computer):
        """Menjalankan permainan."""
        turn = 1

        while not self.end:
            self.check_players_turns(_c)
            self.check_players_hp(_c)
            self.check_players_total_cards(_c)

            self.check_players_turns(_h)
            self.check_players_hp(_h)
            self.check_players_total_cards(_h)

            if self.end:
                break
            else:
                if turn == 1:
                    console.rule("[bold]Giliran Komputer")
                    rprint("Komputer sedang memilih aksi ...")
                    _c.chose_stategy()
                    _c.chose_card()
                    _c.change_card_state()
                    _c.deduct_turn()
                    turn = 2
                else:
                    console.rule("[bold]Giliran Pemain")
                    self.gen_info_table(_h)
                    console.print(self.info_table)
                    _h.gen_cards_table()
                    console.print(_h.cards_table)
                    self.ask_player_to_chose_card(_h)
                    self.ask_player_to_set_card_state(_h)
                    _h.deduct_turn()

                    console.rule("[bold]Arena")
                    self.gen_arena(_h=_h, _c=_c)
                    console.print(self.arena)

                    self.battle(_h=_h, _c=_c)
                    turn = 1

        rprint("Menentukan pemenang ...")
        self.calculate_winner(_h=_h, _c=_c)
        self.declare_winner()

    def lobby(self, _h: player.Human, _c: player.Computer):
        """Game lobby."""
        while self.run:
            console.rule("[bold]Lobby")
            print(rw.read_f("title.txt"))
            console.print(rw.read_md("greet_lobby.md"))
            self.menu(_h=_h, _c=_c)
