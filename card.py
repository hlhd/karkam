"""Module for card related operation in the game."""

import random


class Card:
    """Kelas kartu."""

    def __init__(self):
        """Inisialisasi kartu."""
        self.defence = self.gen_dfs_atk_point()
        self.attack = self.gen_dfs_atk_point()
        # attribut card.state digunakan untuk menentukan apakah kartu dalam
        # mode menyerang atau bertahan, jika False berarti kartu dalam mode
        # bertahan jika True maka kartu dalam mode menyerang
        self.state = False

    def gen_dfs_atk_point(self):
        """Untuk menentukan defence atau attack point pada kartu."""
        return random.randint(5, 20)

    def set_state(self, _state: bool):
        """Untuk mengatur mode kartu."""
        self.state = _state

    def set_id(self, _card_id: int):
        """Untuk mengatur id kartu."""
        self.card_id = _card_id + 1

    def __repr__(self):
        """Memberikan informasi tentang objek yang berikan."""
        card_display_format = f"Card #{self.card_id}: "
        card_display_format += f"(Attack = {self.attack!r}, Defence = {self.defence!r})"

        return card_display_format
