"""The main KarKam module."""

from game import Game
import player


if __name__ == "__main__":
    # Buat objek Game
    game = Game(True)
    # Buat objek pemain (komputer dan manusia)
    computer = player.Computer(game.total_cards, game.turn)
    human = player.Human(game.total_cards, game.turn)

    # jalankan game lobby
    game.lobby(human, computer)
