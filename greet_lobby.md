---
Selamat datang di KarKam permainan adu kartu sederhana yang menyerupai YuGiOh atau Duel Master!  

Anda akan bermain melawan komputer  

Ketentuan dalam permainan:
- Tiap pemain memiliki **100 HP** (Health Point)

  **Pemenang** ditentukan oleh jumlah HP pada akhir permainan
- Tiap pemain akan mendapat 6 - 10 giliran (giliran akan ditentukan secara acak oleh sistem)
   - Giliran akan digunakan untuk memilih kartu, kemudian mengatur mode kartu
   - Permainan akan berakhir ketika masing - masing pemain telah kehabisan giliran atau salah satunya kehabisan HP
- Tiap pemain akan mendapat 5 - 10 kartu untuk di adu (jumlah kartu akan ditentukan secara acak oleh sistem)

   Tiap kartu dapat diatur dalam mode bertahan atau menyerang  
   Tiap kartu akan memiliki 2 status:
   - Attack
   - Defence
   
   Masing - masing status bisa memiliki poin 5 - 20
